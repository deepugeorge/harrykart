package se.atg.service.harrykart.java.service;

import org.springframework.stereotype.Service;
import se.atg.service.harrykart.java.model.*;

import java.util.*;

@Service
public class HarryKartService {

    private static final double TRACK_LENGTH_MTS = 1000;

    public Map<String, List<Ranking>> getRaceWinners(final HarryKart harryKart) {
        final List<RaceResult> racePosition = new ArrayList<>();
        harryKart.getLoops().forEach(loop -> loop.getLanes().forEach(currentLane -> {
            // Get the participant in the current line
            final Optional<Participant> optionalParticipant = harryKart.getParticipants().parallelStream().
                    filter(participant -> participant.getLane() == currentLane.getNumber()).findFirst();
            optionalParticipant.ifPresent(participant -> calculatePosition(participant, racePosition, currentLane));
        }));

        racePosition.sort(Comparator.comparing(RaceResult::getTime));

        return calculateRanking(racePosition);
    }

    private void calculatePosition(final Participant participant, final List<RaceResult> racePosition, final Lane currentLane) {
        Optional<RaceResult> lastLapRankOpt = racePosition.parallelStream().
                filter(ranking -> Objects.equals(ranking.getName(), participant.getName())).findFirst();
        if (lastLapRankOpt.isPresent()) {
            // Calculation for 2nd lap onwards
            final RaceResult lastLapRank = lastLapRankOpt.get();
            final double lastLapTime = lastLapRank.getTime();
            final int powerUpValue = lastLapRank.getPoweredValue() + currentLane.getPowerValue();
            lastLapRank.setPoweredValue(powerUpValue);
            lastLapRank.setTime(lastLapTime + (TRACK_LENGTH_MTS / powerUpValue));
        } else {
            // First lap calculation
            final int powerUpValue = participant.getBaseSpeed() + currentLane.getPowerValue();
            final RaceResult ranking = new RaceResult(participant.getName(), (TRACK_LENGTH_MTS / powerUpValue), powerUpValue);
            racePosition.add(ranking);
        }
    }

    private Map<String, List<Ranking>> calculateRanking(List<RaceResult> racePosition) {
        final List<Ranking> rankings = new ArrayList<>();
        for (int i = 0; i < racePosition.size(); i++) {
            final RaceResult raceResult = racePosition.get(i);
            rankings.add(new Ranking(i + 1, raceResult.getName()));
        }
        return Map.of("ranking", rankings.size() > 3 ? rankings.subList(0, 3) : rankings);
    }
}
