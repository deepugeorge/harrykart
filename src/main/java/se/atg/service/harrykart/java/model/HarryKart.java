package se.atg.service.harrykart.java.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.util.List;

@Data
@JacksonXmlRootElement(localName = "harryKart")
public class HarryKart {
    private int numberOfLoops;

    @JacksonXmlProperty(localName = "startList")
    private List<Participant> participants;

    @JacksonXmlProperty(localName = "powerUps")
    private List<Loop> loops;


}
