package se.atg.service.harrykart.java.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Ranking {
    private int position;
    private String name;
}
