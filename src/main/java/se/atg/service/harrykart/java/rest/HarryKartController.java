package se.atg.service.harrykart.java.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import se.atg.service.harrykart.java.model.HarryKart;
import se.atg.service.harrykart.java.model.Ranking;
import se.atg.service.harrykart.java.service.HarryKartService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/java/api")
public class HarryKartController {

    @Autowired
    HarryKartService harryKartService;

    @PostMapping(path = "/play", consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Map<String,List<Ranking>> playHarryKart(@RequestBody final HarryKart harryKart) {
        return harryKartService.getRaceWinners(harryKart);
    }

}
