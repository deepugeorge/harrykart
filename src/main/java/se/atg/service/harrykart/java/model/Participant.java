package se.atg.service.harrykart.java.model;

import lombok.Data;

@Data
public class Participant {
    private int lane;
    private String name;
    private int baseSpeed;
}
