package se.atg.service.harrykart.java.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RaceResult {
    private String name;
    private double time;
    private int poweredValue;
}
