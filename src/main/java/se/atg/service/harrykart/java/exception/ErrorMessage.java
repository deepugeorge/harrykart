package se.atg.service.harrykart.java.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class ErrorMessage {
    int errorCode;
    Date data;
    String errorMessage;
    String errorDescription;
}
